import 'package:flutter/material.dart';
import './env.dart';

class Nesti extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      routes: routes,
      initialRoute: Router.login,
      debugShowCheckedModeBanner: false,
    );
  }
}
