import 'package:flutter/material.dart';

import '../env.dart';

class LoginView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SingleChildScrollView(
            child: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        SizedBox(
          height: 128.0,
        ),
        Hero(
          tag: "logo",
                  child: Image.asset(
            "assets/logo_blue.png",
            scale: 1.5,
          ),
        ),
        SizedBox(height: 32.0),
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 16.0, horizontal: 32.0),
          child: NestiTextField(
            hintText: "al******@alumnos.uacj.mx",
            labelText: "correo",
          ),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 16.0, horizontal: 32.0),
          child: NestiTextField(
            hintText: "******",
            labelText: "Contraseña",
            secret: true,
          ),
        ),
        SizedBox(height: 32.0),
        NestiButton(
          text: "Login",
          color: Colors.white,
          backgroundColor: Colors.blueAccent,
          onPressed:()=>Navigator.pushNamed(context, Router.splash)
        ),
        SizedBox(height: 32.0),
        Text(
          "Powered by Return Ready",
          style: TextStyle(
            fontSize: 12.0,
            fontWeight: FontWeight.w300,
          ),
        )
      ],
    )));
  }
}
