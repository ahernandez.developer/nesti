import 'package:flutter/material.dart';

class SplashView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Center(
            child: Hero(
              tag: "logo",
              child: Image.asset("assets/logo_blue.png")),
          ),
          SizedBox(
            height: 32.0,
          ),
          Text(
            "Powered by Return Ready",
            style: TextStyle(
              fontSize: 12.0,
              fontWeight: FontWeight.w300,
            ),
          )
        ],
      ),
    );
  }
}
