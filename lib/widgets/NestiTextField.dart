import 'package:flutter/material.dart';

class NestiTextField extends StatelessWidget {
  final String hintText;
  final String labelText;
  final bool secret;
  NestiTextField({this.hintText, this.labelText, this.secret = false, Key key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextField(
      obscureText: this.secret,
      decoration: InputDecoration(
        labelText: this.labelText,
        border: borderStyle,
        enabledBorder: borderStyle,
        disabledBorder: borderStyle,
        focusedBorder: borderStyle,
        focusedErrorBorder: borderStyle,
        errorBorder: borderStyle.copyWith(
          borderSide: BorderSide(
            color: Colors.redAccent,
          ),
        ),
        fillColor: Colors.blueAccent,
        hintText: this.hintText,
        hintStyle: hintStyle,
      ),
    );
  }

  final OutlineInputBorder borderStyle = OutlineInputBorder(
    borderRadius: BorderRadius.circular(16.0),
    borderSide: BorderSide(
      color: Colors.blueAccent,
      width: 2.0,
    ),
  );

  final TextStyle hintStyle = TextStyle(
    color: Colors.black,
    fontWeight: FontWeight.w300,
  );
}
