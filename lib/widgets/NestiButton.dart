import 'package:flutter/material.dart';

class NestiButton extends StatelessWidget {
  final String text;
  final Color color;
  final Color backgroundColor;
  final Function onPressed;
  NestiButton({this.text,this.onPressed, this.color, this.backgroundColor, Key key})
      : super(key: key);
  @override
  build(BuildContext context) {
    return InkWell(
      onTap: this.onPressed,
      child: Container(
        alignment: Alignment.center,
        height: 48.0,
        width: 128.0,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(32.0),
          color: this.backgroundColor,
        ),
        child: Text(
          this.text,
          textAlign: TextAlign.center,
          style: TextStyle(
            color: this.color,
          ),
        ),
      ),
    );
  }
}
