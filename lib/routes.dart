import 'package:flutter/material.dart';

import './views/splash.dart';
import './views/login.dart';

class Router {
  static String splash = "/Splash";
  static String login = "/Login";
}

Map<String, Widget Function(BuildContext context)> routes = {
  Router.splash: (BuildContext context) => SplashView(),
  Router.login: (BuildContext context) => LoginView(),
};
